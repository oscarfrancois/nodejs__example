function compute() {
    let parameter1 = document.getElementById("parameter1").value;
    let parameter2 = document.getElementById("parameter2").value;
    fetch('api/compute?parameter1=' + parameter1 + "&parameter2=" + parameter2)
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            document.getElementById("result").innerHTML = " Result=" + myJson['result'];
        });

}