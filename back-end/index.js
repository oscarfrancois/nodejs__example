/* main back-end file */

// import section
const express = require('express');
const check = require('express-validator');

// instanciate the web server
const app = express();

/* just send HTTP 200 code when /api/status is called */
app.get('/api/status', function (req, res) {
  res.sendStatus(200);
});

/* compute parameter1 * parameter2 and return the result in json format

 * @param {int} parameter1 - the first number to multiply
 * @param {int} parameter2 - the second number to multiply
 * @return {int} the result of the multiplication
 */
app.get('/api/compute', function (req, res) {
  let parameter1 = parseInt(req.query.parameter1);
  let parameter2 = parseInt(req.query.parameter2);

  // filtering input (numeric and integer overflow)
  if (isNaN(parameter1) || isNaN(parameter2) || !Number.isFinite(parameter1 * parameter2)) {
    res.sendStatus(400);
    return;
  }

  // forging and returning the json answer
  ret = {};
  ret['result'] = req.query.parameter1 * req.query.parameter2;
  res.json(ret);
});

/* to serve static content */
app.use(express.static(__dirname + '/..'));

/* listen for client */
const PORT = 8080;
app.listen(PORT);
console.log(`app listening on port ${PORT}`);